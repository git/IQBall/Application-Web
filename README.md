# IQBall - Web Application

This repository hosts the IQBall application for web

## Read the docs !

You can find some additional documentation in the [Documentation](Documentation) folder,
and in the [wiki](https://codefirst.iut.uca.fr/git/IQBall/Application-Web/wiki).
