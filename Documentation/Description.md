# Welcome on the documentation's description

## Let's get started with the architecture diagram.

![architecture diagram](./assets/architecture.svg)

As you can see our entire application is build around three main package.
All of them contained in "src" package.
The core represent the main code of the web application.
It contains all the validation protocol, detailed below, the model of the imposed MVC architecture.
It also has a package named "data", it is a package of the structure of all the data we use in our application.
Of course there is package containing all the gateways as its name indicates. It is where we use the connection to our database.
Allowing to operate on it.

The App now is more about the web application itself.
Having all the controllers of the MVC architecture the use the model, the validation system and the http system in the core.
It also calls the twig's views inside of App. Finally, it uses the package Session. This one replace the $\_SESSION we all know in PHP.
Thanks to this we have a way cleaner use of all session's data.
Nevertheless, all the controllers call not only twig views but also react ones.
Those are present in the package "front", dispatched in several other packages.
Such as assets having all the image and stuff, model containing all the data's structure, style centralizing all css file and eventually components the last package used for the editor.

Finally, we have the package "Api" that allows to share code and bind all the different third-hand application such as the web admin one.

## Main data class diagram.

![Class diagram](./assets/models.svg)

You can see how our data is structured contained in the package "data" as explained right above.
There is two clear part.
First of all, the Tactic one.
We got a nice class named TacticInfo representing as it says the information about a tactic, nothing to discuss more about.
It associates an attribute of type "CourtType". This last is just an "evoluated" type of enum with some more features.
We had to do it this way because of the language PHP that doesn't implement such a thing as an enum.

Now, let's discuss a much bigger part of the diagram.
In this part we find all the team logic. Actually, a team only have an array of members and a "TeamInfo".
The class "TeamInfo" only exists to split the team's information data (name, id etc) from the members.
The type Team does only link the information about a team and its members.
Talking about them, their class indicate what role they have (either Coach or Player) in the team.
Because a member is registered in the app, therefore he is a user of it. Represented by the type of the same name.
This class does only contain all the user's basic information.
The last class we have is the Account. It could directly be incorporated in User but we decided to split it the same way we did for the team.
Then, Account only has a user and a token which is an identifier.

## Validation's class diagram

![validation's class diagram](./assets/validation.svg)

We implemented our own validation system, here it is!
For the validation methods (for instance those in DefaultValidators) we use lambda to instantiate a Validator.
In general, we use the implementation "SimpleFunctionValidator".
We reconize the strategy pattern. Indeed, we need a family of algorithms because we have many classes that only differ by the way they validate.
Futhermore, you may have notices the ComposedValidator that allows to chain several Validator.
We can see that this system uses the composite pattern
The other part of the diagram is about the failure a specific field's validation.
We have a concrete class to return a something more general. All the successors are just more precise about the failure.

## Http's class diagram

![Http's class diagram](./assets/http.svg)
It were we centralize what the app can render, and what the api can receive.
Then, we got the "basic" response (HttpResponse) that just render a HttpCodes.
We have two successors for now. ViewHttpResponse render not only a code but also a view, either react or twig ones.
Finally, we have the JsonHttpResponse that renders, as it's name says, some Json.

## Session's class diagram

![Session's class diagram](./assets/session.svg)

It encapsulates the PHP's array "$\_SESSION". With two interfaces that dictate how a session should be handled, and same for a mutable one.

## Model View Controller

All class diagram, separated by their range of action, of the imposed MVC architecture.
All of them have a controller that validates entries with the validation system and check the permission the user has,and whether or not actually do the action.
These controllers are composed by a Model that handle the pure data and is the point of contact between these and the gateways.
Speaking of which, Gateways are composing Models. They use the connection class to access the database and send their query.

### Team

![team's mvc](./assets/team.svg)

### Editor

![editor's mvc](./assets/editor.svg)

### Authentification

![auth's mvc](./assets/auth.svg)
