import AccountSvg from "../../assets/account.svg?react"
import "../../style/template/header.css"
import { useUser } from "../../App.tsx"
import { useNavigate } from "react-router-dom"

export function Header() {
    const navigate = useNavigate()
    const [user] = useUser()

    const accountImage = user?.profilePicture ? (
        <img
            id="img-account"
            src={user.profilePicture}
            alt={`Photo de profil de ${user!.name}`}
        />
    ) : (
        <AccountSvg id="img-account" />
    )

    return (
        <div id="header">
            <div id="header-left"></div>
            <div id="header-center">
                <p
                    id="iqball"
                    className="clickable"
                    onClick={() => navigate("/")}>
                    IQBall
                </p>
            </div>
            <div id="header-right">
                <div
                    className="clickable"
                    id="clickable-header-right"
                    onClick={() => {
                        if (user) {
                            navigate("/settings")
                            return
                        }

                        navigate("/login")
                    }}>
                    {accountImage}
                    <p id="username">{user?.name ?? "Log In"}</p>
                </div>
            </div>
        </div>
    )
}
