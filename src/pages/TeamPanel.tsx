import "../style/team_panel.css"
import { BASE } from "../Constants"
import { Member, Team, TeamInfo } from "../model/Team"
import { useParams } from "react-router-dom"

export default function TeamPanelPage() {
    const { teamId } = useParams()
    const teamInfo = {
        id: parseInt(teamId!),
        name: teamId!,
        mainColor: "#FFFFFF",
        secondColor: "#000000",
        picture:
            "https://a.espncdn.com/combiner/i?img=/i/teamlogos/nba/500/lal.png",
    }
    return (
        <TeamPanel
            team={{ info: teamInfo, members: [] }}
            currentUserId={0}
            isCoach={false}
        />
    )
}

function TeamPanel({
    isCoach,
    team,
    currentUserId,
}: {
    isCoach: boolean
    team: Team
    currentUserId: number
}) {
    return (
        <div id="main-div">
            <header>
                <h1>
                    <a href={BASE + "/"}>IQBall</a>
                </h1>
            </header>
            <TeamDisplay team={team.info} />

            {isCoach && <CoachOptions id={team.info.id} />}

            <MembersDisplay
                members={team.members}
                isCoach={isCoach}
                idTeam={team.info.id}
                currentUserId={currentUserId}
            />
        </div>
    )
}

function TeamDisplay({ team }: { team: TeamInfo }) {
    return (
        <div id="team-info">
            <div id="first-part">
                <h1 id="team-name">{team.name}</h1>
                <img id="logo" src={team.picture} alt="Logo d'équipe" />
            </div>
            <div id="colors">
                <div id="colorsTitle">
                    <p>Couleur principale</p>
                    <p>Couleur secondaire</p>
                </div>
                <div id="actual-colors">
                    <ColorDisplay color={team.mainColor} />
                    <ColorDisplay color={team.secondColor} />
                </div>
            </div>
        </div>
    )
}

function ColorDisplay({ color }: { color: string }) {
    return <div className="square" style={{ backgroundColor: color }} />
}

function CoachOptions({ id }: { id: number }) {
    return (
        <div>
            <button
                id="delete"
                onClick={() =>
                    confirm("Êtes-vous sûr de supprimer cette équipe?")
                        ? (window.location.href = `${BASE}/team/${id}/delete`)
                        : {}
                }>
                Supprimer
            </button>
            <button
                id="edit"
                onClick={() =>
                    (window.location.href = `${BASE}/team/${id}/edit`)
                }>
                Modifier
            </button>
        </div>
    )
}

function MembersDisplay({
    members,
    isCoach,
    idTeam,
    currentUserId,
}: {
    members: Member[]
    isCoach: boolean
    idTeam: number
    currentUserId: number
}) {
    const listMember = members.map((member) => (
        <MemberDisplay
            member={member}
            isCoach={isCoach}
            idTeam={idTeam}
            currentUserId={currentUserId}
        />
    ))
    return (
        <div id="members">
            <div id="head-members">
                <h2>Membres :</h2>
                {isCoach && (
                    <button
                        id="add-member"
                        onClick={() =>
                            (window.location.href = `${BASE}/team/${idTeam}/addMember`)
                        }>
                        +
                    </button>
                )}
            </div>
            {listMember}
        </div>
    )
}

function MemberDisplay({
    member,
    isCoach,
    idTeam,
    currentUserId,
}: {
    member: Member
    isCoach: boolean
    idTeam: number
    currentUserId: number
}) {
    return (
        <div className="member">
            <img
                id="profile-picture"
                src={member.user.profilePicture}
                alt="Photo de profile"
            />
            <p>{member.user.name}</p>
            <p>{member.role}</p>
            <p>{member.user.email}</p>
            {isCoach && currentUserId !== member.user.id && (
                <button
                    id="delete"
                    onClick={() =>
                        confirm(
                            "Êtes-vous sûr de retirer ce membre de l'équipe?",
                        )
                            ? (window.location.href = `${BASE}/team/${idTeam}/remove/${member.user.id}`)
                            : {}
                    }>
                    Retirer
                </button>
            )}
            {isCoach && currentUserId == member.user.id && (
                <button
                    id="delete"
                    onClick={() =>
                        confirm("Êtes-vous sûr de quitter cette équipe?")
                            ? (window.location.href = `${BASE}/team/${idTeam}/remove/${member.user.id}`)
                            : {}
                    }>
                    Quitter
                </button>
            )}
        </div>
    )
}
