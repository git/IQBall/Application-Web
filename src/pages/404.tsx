import { useLocation } from "react-router-dom"
import { BASE } from "../Constants.ts"
export default function NotFoundPage() {
    const target = useLocation()

    return (
        <div>
            <h1>{target.pathname} NOT FOUND !</h1>
            <button onClick={() => (location.pathname = BASE + "/")}>
                go back to home
            </button>
        </div>
    )
}
