/**
 * information about a player that is into a rack
 */
import { PlayerTeam } from "../model/tactic/Player"

export interface RackedPlayer {
    team: PlayerTeam
    key: string
}

export type RackedCourtObject = { key: "ball" }
