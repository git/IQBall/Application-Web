import { StepContent } from "../model/tactic/TacticInfo.ts"

export class ContentVersions {
    private index = 0
    private contents: StepContent[] = []

    public insertAndCut(content: StepContent) {
        this.contents.splice(this.index + 1)
        this.contents.push(content)
        this.index = this.contents.length - 1
    }

    public previous(): StepContent | null {
        if (this.index == 0) return null
        return this.contents[--this.index]
    }

    public next(): StepContent | null {
        if (this.index == this.contents.length - 1) return null
        return this.contents[++this.index]
    }
}
