import "../../style/actions/arrow_action.css"
import Draggable from "react-draggable"
import arrowPng from "../../assets/icon/arrow.svg"
import { useRef } from "react"

export interface ArrowActionProps {
    onHeadDropped: (headBounds: DOMRect) => void
    onHeadPicked: (headBounds: DOMRect) => void
    onHeadMoved: (headBounds: DOMRect) => void
}

export default function ArrowAction({
    onHeadDropped,
    onHeadPicked,
    onHeadMoved,
}: ArrowActionProps) {
    const arrowHeadRef = useRef<HTMLDivElement>(null)

    return (
        <div className="arrow-action">
            <img className="arrow-action-icon" src={arrowPng} alt="add arrow" />

            <Draggable
                nodeRef={arrowHeadRef}
                onStart={() => {
                    const headBounds =
                        arrowHeadRef.current!.getBoundingClientRect()
                    onHeadPicked(headBounds)
                }}
                onStop={() => {
                    const headBounds =
                        arrowHeadRef.current!.getBoundingClientRect()
                    onHeadDropped(headBounds)
                }}
                onDrag={() => {
                    const headBounds =
                        arrowHeadRef.current!.getBoundingClientRect()
                    onHeadMoved(headBounds)
                }}
                position={{ x: 0, y: 0 }}>
                <div ref={arrowHeadRef} className="arrow-head-pick" />
            </Draggable>
        </div>
    )
}

export function ScreenHead({ color }: { color: string }) {
    return (
        <div style={{ backgroundColor: color, height: "5px", width: "25px" }} />
    )
}

export function MoveToHead({ color }: { color: string }) {
    return (
        <svg viewBox={"0 0 50 50"} width={20} height={20}>
            <polygon points={"50 0, 0 0, 25 40"} fill={color} />
        </svg>
    )
}
