import { BallPiece } from "../editor/BallPiece"
import Draggable from "react-draggable"
import { useRef } from "react"
import { NULL_POS } from "../../geo/Pos"

export interface BallActionProps {
    onDrop: (el: DOMRect) => void
}

export default function BallAction({ onDrop }: BallActionProps) {
    const ref = useRef<HTMLDivElement>(null)
    return (
        <Draggable
            nodeRef={ref}
            onStop={() => onDrop(ref.current!.getBoundingClientRect())}
            position={NULL_POS}>
            <div ref={ref}>
                <BallPiece />
            </div>
        </Draggable>
    )
}
