import { CSSProperties, useRef, useState } from "react"
import "../style/title_input.css"

export interface TitleInputOptions {
    style: CSSProperties
    default_value: string
    onValidated: (a: string) => void
}

export default function TitleInput({
    style,
    default_value,
    onValidated,
}: TitleInputOptions) {
    const [value, setValue] = useState(default_value)
    const ref = useRef<HTMLInputElement>(null)

    return (
        <input
            className="title-input"
            ref={ref}
            style={style}
            type="text"
            value={value}
            onChange={(event) => setValue(event.target.value)}
            onBlur={() => onValidated(value)}
            onKeyUp={(event) => {
                if (event.key == "Enter") ref.current?.blur()
            }}
        />
    )
}
