import { Action, ActionKind } from "../../model/tactic/Action"
import BendableArrow from "../arrows/BendableArrow"
import { RefObject } from "react"
import { MoveToHead, ScreenHead } from "../actions/ArrowAction"
import { ComponentId } from "../../model/tactic/TacticInfo.ts"

export interface CourtActionProps {
    origin: ComponentId
    action: Action
    color: string
    courtRef: RefObject<HTMLElement>
    isEditable: boolean
    onActionChanges?: (a: Action) => void
    onActionDeleted?: () => void
}

export function CourtAction({
    origin,
    action,
    color,
    onActionChanges,
    onActionDeleted,
    courtRef,
    isEditable,
}: CourtActionProps) {
    let head
    switch (action.type) {
        case ActionKind.DRIBBLE:
        case ActionKind.MOVE:
        case ActionKind.SHOOT:
            head = () => <MoveToHead color={color} />
            break
        case ActionKind.SCREEN:
            head = () => <ScreenHead color={color} />
            break
    }

    let dashArray
    switch (action.type) {
        case ActionKind.SHOOT:
            dashArray = "10 5"
            break
    }

    return (
        <BendableArrow
            forceStraight={action.type == ActionKind.SHOOT}
            area={courtRef}
            startPos={origin}
            segments={action.segments}
            onSegmentsChanges={(edges) => {
                if (onActionChanges)
                    onActionChanges({ ...action, segments: edges })
            }}
            wavy={action.type == ActionKind.DRIBBLE}
            readOnly={!isEditable}
            //TODO place those magic values in constants
            endRadius={action.target ? 26 : 17}
            startRadius={10}
            onDeleteRequested={onActionDeleted}
            style={{
                head,
                dashArray,
                color,
            }}
        />
    )
}
