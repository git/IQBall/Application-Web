export interface SaveState {
    className: string
    message: string
}

export class SaveStates {
    static readonly Guest: SaveState = {
        className: "save-state-guest",
        message:
            "vous n'etes pas connectés, les changements seront sauvegardés sur votre navigateur.",
    }
    static readonly Ok: SaveState = {
        className: "save-state-ok",
        message: "sauvegardé",
    }
    static readonly Saving: SaveState = {
        className: "save-state-saving",
        message: "sauvegarde...",
    }
    static readonly Err: SaveState = {
        className: "save-state-error",
        message: "erreur lors de la sauvegarde.",
    }
}

export default function SavingState({ state }: { state: SaveState }) {
    return (
        <div className={"save-state"}>
            <div className={state.className}>{state.message}</div>
        </div>
    )
}
