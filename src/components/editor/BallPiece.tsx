import "../../style/ball.css"

import BallSvg from "../../assets/icon/ball.svg?react"
import { BALL_ID } from "../../model/tactic/CourtObjects"

export function BallPiece() {
    return <BallSvg id={BALL_ID} className={"ball"} />
}
