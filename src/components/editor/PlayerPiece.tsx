import "../../style/player.css"
import { PlayerTeam } from "../../model/tactic/Player"

export interface PlayerPieceProps {
    team: PlayerTeam
    text: string
    hasBall: boolean
}

export function PlayerPiece({ team, text, hasBall }: PlayerPieceProps) {
    let className = `player-piece ${team}`
    if (hasBall) {
        className += " player-piece-has-ball"
    } else {
        className += " player-piece-has-no-ball"
    }

    return (
        <div className={className}>
            <p>{text}</p>
        </div>
    )
}
