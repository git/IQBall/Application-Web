import { Pos } from "./Pos"

export interface Box {
    x: number
    y: number
    width: number
    height: number
}

export function boundsOf(...positions: Pos[]): Box {
    const allPosX = positions.map((p) => p.x)
    const allPosY = positions.map((p) => p.y)

    const x = Math.min(...allPosX)
    const y = Math.min(...allPosY)
    const width = Math.max(...allPosX) - x
    const height = Math.max(...allPosY) - y

    return { x, y, width, height }
}

export function surrounds(pos: Pos, width: number, height: number): Box {
    return {
        x: pos.x + width / 2,
        y: pos.y + height / 2,
        width,
        height,
    }
}

export function overlaps(a: Box, b: Box): boolean {
    if (a.x + a.width < b.x || b.x + b.width < a.x) {
        return false
    }

    return !(a.y + a.height < b.y || b.y + b.height < a.y)
}

export function contains(box: Box, pos: Pos): boolean {
    return (
        pos.x >= box.x &&
        pos.x <= box.x + box.width &&
        pos.y >= box.y &&
        pos.y <= box.y + box.height
    )
}
