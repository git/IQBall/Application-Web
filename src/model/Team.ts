import { User } from "./User"

export interface TeamInfo {
    id: number
    name: string
    picture: string
    mainColor: string
    secondColor: string
}

export interface Team {
    info: TeamInfo
    members: Member[]
}

export interface Member {
    user: User
    role: string
}
