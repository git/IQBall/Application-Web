import { Player, PlayerPhantom } from "./Player"
import { Action } from "./Action"
import { CourtObject } from "./CourtObjects"

export interface Tactic {
    readonly name: string
    readonly courtType: CourtType
    readonly root: TacticStep
}

export interface TacticInfo {
    readonly id: number
    readonly name: string
    readonly courtType: CourtType
    readonly rootStepNode: StepInfoNode
}

export interface TacticStep {
    readonly content: StepContent
    readonly children: TacticStep[]
}

export interface StepContent {
    readonly components: TacticComponent[]
}

export interface StepInfoNode {
    readonly id: number
    readonly children: StepInfoNode[]
}

export type TacticComponent = Player | CourtObject | PlayerPhantom
export type ComponentId = string
export type CourtType = "PLAIN" | "HALF"

export interface Component<T, Positioning> {
    /**
     * The component's type
     */
    readonly type: T
    /**
     * The component's identifier
     */
    readonly id: ComponentId

    readonly pos: Positioning

    readonly actions: Action[]
}

export interface Frozable {
    readonly frozen: boolean
}
