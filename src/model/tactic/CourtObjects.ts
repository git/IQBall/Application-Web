import { Component, Frozable } from "./TacticInfo.ts"
import { Pos } from "../../geo/Pos.ts"

export const BALL_ID = "ball"
export const BALL_TYPE = "ball"

export type Ball = Component<typeof BALL_TYPE, Pos> & Frozable

//place here all different kinds of objects
export type CourtObject = Ball
