import { Pos } from "../../geo/Pos"
import { ComponentId } from "./TacticInfo.ts"

export enum ActionKind {
    SCREEN = "SCREEN",
    DRIBBLE = "DRIBBLE",
    MOVE = "MOVE",
    SHOOT = "SHOOT",
}

export type Action = MovementAction

export interface Segment {
    next: Pos | string
    controlPoint?: Pos
}

export interface MovementAction {
    type: ActionKind
    target: ComponentId | Pos
    segments: Segment[]
}

export function moves(kind: ActionKind): boolean {
    return kind != ActionKind.SHOOT
}
