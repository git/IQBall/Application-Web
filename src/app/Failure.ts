export interface Failure {
    type: string
    messages: string[]
}
