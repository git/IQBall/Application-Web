import {
    CourtType,
    StepContent,
    StepInfoNode,
} from "../model/tactic/TacticInfo.ts"

export interface TacticContext {
    stepsTree: StepInfoNode
    name: string
    courtType: CourtType
}

export enum ServiceError {
    UNAUTHORIZED = "UNAUTHORIZED",
    NOT_FOUND = "NOT_FOUND",
}

export interface TacticService {
    getContext(): Promise<TacticContext | ServiceError>
    getContent(step: number): Promise<StepContent | ServiceError>
    canBeEdited(): Promise<boolean>
}

export interface MutableTacticService extends TacticService {
    addStep(
        parentId: number,
        content: StepContent,
    ): Promise<StepInfoNode | ServiceError>

    removeStep(id: number): Promise<void | ServiceError>

    setName(name: string): Promise<void | ServiceError>

    saveContent(
        step: number,
        content: StepContent,
    ): Promise<void | ServiceError>
}
