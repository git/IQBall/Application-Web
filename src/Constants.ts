/**
 * This constant defines the API endpoint.
 */
export const API = import.meta.env.VITE_API_ENDPOINT

/**
 * This constant defines the base app's endpoint.
 */
export const BASE = import.meta.env.BASE_URL.slice(
    0,
    import.meta.env.BASE_URL.length - 1,
)
