import {
    CourtType,
    StepContent,
    StepInfoNode,
} from "../model/tactic/TacticInfo.ts"
import { useReducer } from "react"

export interface VisualizerState {
    stepId: number
    tacticName: string
    courtType: CourtType
    stepsTree: StepInfoNode
    content: StepContent
    parentContent: StepContent | null
}

export const enum VisualizerStateActionKind {
    INIT,
    SET_CONTENTS,
}

export type VisualizerStateAction =
    | {
          type: VisualizerStateActionKind.INIT
          state: VisualizerState
      }
    | {
          type: VisualizerStateActionKind.SET_CONTENTS
          content: StepContent
          parentContent: StepContent | null
          stepId: number
      }

export function useVisualizer(initialState: VisualizerState | null) {
    return useReducer(visualizerStateReducer, initialState)
}

function visualizerStateReducer(
    state: VisualizerState | null,
    action: VisualizerStateAction,
): VisualizerState | null {
    switch (action.type) {
        case VisualizerStateActionKind.INIT:
            return action.state
        case VisualizerStateActionKind.SET_CONTENTS:
            if (state === null) throw Error("State is uninitialized !")
            return {
                ...state,
                stepId: action.stepId,
                content: action.content,
                parentContent: action.parentContent,
            }
    }
}
