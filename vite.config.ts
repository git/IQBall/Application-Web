import { defineConfig } from "vite"
import react from "@vitejs/plugin-react"
import cssInjectedByJsPlugin from "vite-plugin-css-injected-by-js"
import svgr from "vite-plugin-svgr"
import { visualizer } from "rollup-plugin-visualizer"

// https://vitejs.dev/config/
export default defineConfig({
    build: {
        target: "es2021",
        cssTarget: ["chrome112"]
    },
    test: {
        environment: "jsdom",
    },
    plugins: [
        react(),
        cssInjectedByJsPlugin({
            relativeCSSInjection: true,
        }),
        svgr(),
        visualizer()
    ],
})
