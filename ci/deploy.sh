set -eu

mkdir ~/.ssh
echo "$SERVER_PRIVATE_KEY" > ~/.ssh/id_rsa
chmod 0600 ~/.ssh
chmod 0500 ~/.ssh/id_rsa*

SERVER_ROOT=/srv/www/iqball

ssh -p 80 -o 'StrictHostKeyChecking=no' iqball@maxou.dev mkdir -p $SERVER_ROOT/$DRONE_BRANCH
rsync -avz -e "ssh -p 80 -o 'StrictHostKeyChecking=no'" --delete /outputs/* iqball@maxou.dev:$SERVER_ROOT/$DRONE_BRANCH
ssh -p 80 -o 'StrictHostKeyChecking=no' iqball@maxou.dev "chmod 777 $SERVER_ROOT/$DRONE_BRANCH"