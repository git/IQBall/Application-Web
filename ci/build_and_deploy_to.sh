#!/usr/bin/env bash
set -xeu

export OUTPUT=$1
export BASE=$2

rm -rf "$OUTPUT"/*

echo "VITE_API_ENDPOINT=$BASE/api" >> .env.PROD
echo "VITE_BASE=$BASE" >> .env.PROD

ci/build_react.msh

mkdir -p "$OUTPUT"/profiles/

sed -E 's/\/\*PROFILE_FILE\*\/\s*".*"/"profiles\/prod-config-profile.php"/' config.php > "$OUTPUT"/config.php
sed -E "s/const BASE_PATH = .*;/const BASE_PATH = \"$(sed s/\\//\\\\\\//g <<< "$BASE")\";/" profiles/prod-config-profile.php > $OUTPUT/profiles/prod-config-profile.php

cp -r vendor sql src public "$OUTPUT"
